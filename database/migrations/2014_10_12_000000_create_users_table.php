<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->string('name');				// 姓名
			$table->string('email')->unique();	// Email
			$table->string('password', 60);		// 密碼
			$table->string('job');				// 職稱
			$table->string('phone');			// 電話
			$table->string('lab');				// 實驗室
			$table->string('web');				// 網站
			$table->string('research');			// 研究

			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
