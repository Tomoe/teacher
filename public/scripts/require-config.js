//require-config.js

requirejs.config({

    // 載入模組
    paths: {
        // Core
        'jquery': '/bower_components/jquery/dist/jquery',
        'bootstrap': '/bower_components/bootstrap/dist/js/bootstrap',
        'text': '/bower_components/requirejs-text/text',
        'async': '/bower_components/async/lib/async',
        // Angular
        'angular': '/bower_components/angularjs/angular',
        'angularAnimate': '/bower_components/angular-animate/angular-animate',
        'angularUiRouter': '/bower_components/angular-ui-router/release/angular-ui-router',
        'angularLoadingBar': '/bower_components/angular-loading-bar/build/loading-bar',
        'angularInOut': '/bower_components/angular-ui-router-anim-in-out/anim-in-out',
        'satellizer': '/bower_components/satellizer/satellizer'
    },

    priority: [
        "angular"
    ],

    shim: {

        'bootstrap' : ['jquery'],

        'angular' : { exports: 'angular' },

        "angularAnimate": {
            deps: ["angular"]
        },

        "angularUiRouter": {
            deps: ["angular"]
        },

        "angularLoadingBar": {
            deps: ["angular"]
        },

        "angularInOut": {
            deps: ["angular"]
        },

        "satellizer": {
            deps: ["angular"]
        }

    },

    deps: ['/scripts/bootstrap.js']
});

