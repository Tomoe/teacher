// typeService.js

define(
    [
        './module'
    ]
    ,function(services) {
        
        services.factory('Type', [
            '$http',
            function($http) {
                return {
                    // 取得全部
                    all: function() {
                        return $http.get('/api/types');
                    },

                    // 取得特定ID
                    find: function(id) {
                        return $http.get('/api/types/' + id);
                    }, 
                }
            }
        ]);
        
    }
);