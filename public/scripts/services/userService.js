// userService.js

define(
    [
        './module'
    ]
    ,function(services) {

        services.factory('User', [
            '$http',
            function($http) {
                return {
                    // 取得全部
                    all: function() {
                        return $http.get('/api/users');
                    },

                    // 取得特定ID
                    find: function(id) {
                        return $http.get('/api/users/' + id);
                    }

                }
            }
        ]);

    }
);
