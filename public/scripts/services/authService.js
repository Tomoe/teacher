// userService.js

define(
    [
        './module'
    ]
    ,function(services) {

        services.factory('Auth', [
            '$http',
            function($http) {
                return {
                    // 取得全部
                    currentUser: function() {
                        return $http.get('/api/auth/currentUser');
                    },
                }
            }
        ]);

    }
);
