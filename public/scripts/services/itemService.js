// itemService.js

define(
    [
        './module'
    ]
    ,function(services) {
        
        services.factory('Item', [
            '$http',
            function($http) {
                return {
                    // 取得全部
                    all: function() {
                        return $http.get('/api/items');
                    },

                    // 取得特定ID
                    find: function(id) {
                        return $http.get('/api/items/' + id);
                    }, 

                    getByUserId: function(user_id) {
                        return $http.get('/api/users/' + user_id + '/items');
                    },

                    getByUserIdAndTypeId: function(user_id, type_id) {
                        return $http.get('/api/users/' + user_id + '/items/type/' + type_id);
                    },
                }
            }
        ]);
        
    }
);