define(
  [
    './app'
  ],
  function(app) {
        return app.config(['$stateProvider', '$urlRouterProvider',
            function($stateProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise('/teachers');

                $stateProvider

                    .state('teacherIndex', {
                        url: '/teachers',
                        templateUrl: '/scripts/views/partial-teacher-index.html',
                        controller: 'UserIndexController'
                    })

                    .state('teacherShow', {
                        url: '/teachers/:id',
                        templateUrl: '/scripts/views/partial-teacher-show.html',
                        controller: 'UserShowController'
                    })

                    .state('login', {
                        url: '/auth/login',
                        templateUrl: '/scripts/views/partial-auth-login.html',
                        controller: 'AuthController'
                    })
            }
        ]);
  }
);
