<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>教師平台</title>

	<link href="/css/animate.css" rel="stylesheet">
	<link href="/css/app.css" rel="stylesheet">
	<link href="/css/main.css" rel="stylesheet">
	<link href="/bower_components/angular-loading-bar/build/loading-bar.css" rel="stylesheet">
	<link href="/bower_components/angular-ui-router-anim-in-out/css/anim-in-out.css" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body ng-controller="MainController">

	<!-- Menu -->
	<nav class="navbar navbar-white">
	  <div class="container">
	    <div class="navbar-header">
	      <a class="navbar-brand" ui-sref="teacherIndex()">教師平台</a>
	    </div>

	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav navbar-right">
	        <li ng-if="!isAuthenticated()"><a ui-sref="login()">登入</a></li>
			<li ng-if="isAuthenticated()"><span><span ng-model="currentUser.name"></span>老師，您好</span></li>
			<li ng-if="isAuthenticated()"><a href="#" ng-click="logout()">登出</a></li>
	      </ul>
	    </div>
	  </div>
	</nav>

	<!-- Angular Main -->
	<div class="container">
		<div ui-view></div>
	</div>

	<!-- Scripts -->
	<script src="../bower_components/requirejs/require.js" data-main="../scripts/require-config.js"></script>
</body>
</html>
