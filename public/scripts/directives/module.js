// module.js

define(
  [
    'angular'
  ]
  ,function(ng) {
    return ng.module('app.directives', []);
  }
);