//bootstrap.js

define([
	'require',
    'angular',
    'app',
    'angularAnimate',
    'angularUiRouter',
    'angularLoadingBar',
    'angularInOut',
    'satellizer',
    'routes'
    ], function(require, angular) {
        var $html = angular.element(document.getElementsByTagName('html')[0]);
        angular.element(document).ready(function() {
            angular.bootstrap(document, ['app']);
        });
    }
);
