define(
    [
        'angular'
        ,'./controllers/_index'
        ,'./services/_index'
        ,'./directives/_index'
    ]
  ,function(ng) {

    var app = ng.module('app', [
        'app.controllers'
        ,'app.services'
        ,'app.directives'
        ,'ui.router'
        ,'ngAnimate'
        ,'ngAnimate'
        ,'anim-in-out'
        ,'satellizer'
    ])
    .config(function($authProvider) {

        $authProvider.loginUrl = '/api/auth/login'; // 後端登入網址
        $authProvider.loginRedirect = '/teachers';  // 登入後轉址網址
        $authProvider.loginRoute = '/auth/login';
        $authProvider.withCredentials = true;
    })
    .run(function($rootScope, $window, $auth, Auth) {
      if ($auth.isAuthenticated()) {
          $rootScope.currentUser = $window.localStorage.currentUser;
      }
    });

    return app;
  }
);
