// mainConroller.js

define(
  [
    './module'
  ]
  ,function(controllers) {

        return controllers.controller('MainController', ['$rootScope', '$scope', '$auth',
            function($rootScope, $scope, $auth) {

                // 驗證是否已經登入
                $scope.isAuthenticated = function() {
                    return $auth.isAuthenticated();
                };

                $scope.user = $rootScope.user;

                $scope.logout = function() {
                    $auth.logout();
                }

            }
        ]);
  }
);
