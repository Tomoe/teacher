// mainConroller.js

define(
  [
    './module',
    'async'
  ]
  ,function(controllers, async) {

        controllers.controller('UserIndexController',

            ['$scope', 'User', 'Item', 'Type',

            function($scope, User, Item) {
                User.all().success(function(data){
                    $scope.users = data;
                });
            }
        ]);

        controllers.controller('UserShowController',

            ['$scope', 'User', 'Item', 'Type', '$stateParams',

            function($scope, User, Item, Type, $stateParams, $q) {

                async.waterfall([
                    // 取得 User
                    function(callback) {
                        User.find($stateParams.id)
                        .error(function() {
                            callback("取得User失敗", null)
                        })
                        .success(function(data) {
                            callback(null, data);
                        });
                    },

                    // 取得 Type
                    function(user_data, callback) {
                        $scope.user = user_data;
                        Type.all()
                        .error(function() {
                            callback("取得Type失敗", null);
                        })
                        .success(function(data) {
                            callback(null, data);
                        });

                    },
                    // 取得每個Type的Item
                    function(type_data, callback) {
                        $scope.types = type_data;

                        //遍歷所有Type
                        async.each($scope.types,
                            //
                            function(type, error_callback) {
                                Item.getByUserIdAndTypeId($scope.user.id, type.id)
                                .error(function() {
                                    error_callback("取得Item失敗");
                                })
                                .success(function(item_data) {
                                    var items = item_data;
                                    type["items"] = items;
                                    error_callback();
                                });
                            },
                            // 錯誤處理
                            function(err) {
                                if(err) cosole.log("[ERROR]" + err);
                                else callback(null, 'Done');
                            }
                        );

                    }
                ], function (err, result) {
                    if(err) return cosole.log("[ERROR]" + err);
               });

            }
        ]);

        return controllers;
  }
);
