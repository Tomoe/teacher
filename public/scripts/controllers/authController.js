// mainConroller.js

define(
  [
    './module',
    'async'
  ]

  // controllers : controller 集合
  // async : Async處理函式庫
  ,function(controllers, async) {

    // AuthController
    // 負責處理驗證相關事務

    controllers.controller('AuthController',

      // $scope :
      // $auth : satellizer
      ['$rootScope', '$scope', '$window', '$auth',

      function($rootScope, $scope, $window, $auth) {

        // 測試資料 //////////////////
        $scope.email = "ctchen@nuu.edu.tw";
        $scope.password = "0000";
        //////////////////////////////

        // 登入
        $scope.login = function() {
            $auth.login({ email: $scope.email, password: $scope.password })
            .then(function(response) {
                var user = JSON.parse(response.data.user);
                $window.localStorage.currentUser = user;
                $rootScope.currentUser = user;
                console.log(user.name);
            });
        };

        // 驗證
        $scope.authenticate = function(provider) {
            $auth.authenticate(provider);
        };

      }

    ]);

    return controllers;
  }
);
