<?php namespace App\Http\Controllers;

use App\Models\User;

class UserController extends Controller {

	public function __construct()
	{
		// 驗證權限
		// $this->middleware('auth');
	}

	public function index() {
		return response()->json(User::get());
	}

	public function show($id) {
		return response()->json(User::find($id));
	}

	public function store() {

	}

	public function destroy() {

	}
}
