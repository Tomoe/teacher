<?php namespace App\Http\Controllers;

use App\Models\Type;

class TypeController extends Controller {

	public function __construct()
	{
		// 驗證權限 
		// $this->middleware('auth');
	}

	// Restful

	public function index() {
		return response()->json(Type::get());
	}

	public function show($id) {
		return response()->json(Type::find($id));
	}

}
