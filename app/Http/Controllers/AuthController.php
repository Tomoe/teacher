<?php namespace App\Http\Controllers;

use Request;
use Hash;
use JWTAuth;
use Debugbar;
use Log;

use App\Models\User;

class AuthController extends Controller {

	public function __construct()
	{
		// 驗證權限
		// $this->middleware('auth');
	}

	public function authenticate()
	{

        $email = Request::input('email');
        $password = Request::input('password');
        $user = User::where('email', '=', $email)->first();

        if (!$user)
        {
            return response()->json(array('message' => '找不到此Email的帳號'), 401);
        }

        if (Hash::check($password, $user->password))
        {
            unset($user->password);
            return response()->json(
				array('token' => JWTAuth::fromUser($user),
					  'user' => $user->toJsON()));
        }
        else
        {
            return response()->json(array('message' => '密碼錯誤'), 401);
        }
	}

	public function currentUser() {
		$token = explode(' ', Request::header('Authorization'))[1];
		$payloadObject = JWTAuth::decode($token);
        //$payload = json_decode(json_encode($payloadObject), true);
		Log::info($payloadObject);
        //$user = User::find($payload['id']);

		return response()->json(null);
	}

}
