<?php namespace App\Http\Controllers;

use App\Models\Item;

class ItemController extends Controller {

	public function __construct()
	{
		// 驗證權限 
		// $this->middleware('auth');
	}

	// Restful

	public function index() {
		return response()->json(Item::get());
	}

	public function show($id) {
		return response()->json(Item::find($id));
	}

	public function store() {

	}

	//

	public function getItems($user_id) {
		return response()->json(Item::where("user_id", "=", $user_id)->get());
	}

	public function getItemById($user_id, $item_id) {
		return response()->json(Item::where("user_id", "=", $user_id)->where("id", "=", $item_id)->get());
	}

	public function getItemsByType($user_id, $type_id) {
		return response()->json(Item::where("user_id", "=", $user_id)->where("type_id", "=", $type_id)->get());
	}

}
