<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Models\User;

Route::get('/', 'HomeController@index');

Route::group(array('prefix' => 'api'), function() {

	Route::post('auth/login', 'AuthController@authenticate');

	Route::get('auth/currentUser', array('before' => 'auth', 'uses' => 'AuthController@currentUser'));

	Route::get('users/{user_id}/items', 'ItemController@getItems');
	Route::get('users/{user_id}/items/{item_id}', 'ItemController@getItem');
	Route::get('users/{user_id}/items/type/{type_id}', 'ItemController@getItemsByType');

    Route::resource('users', 'UserController',
        array('only' => array('index', 'show', 'store', 'destroy')));

	Route::resource('items', 'ItemController',
	  array('only' => array('index', 'show', 'store', 'destroy')));

	Route::resource('types', 'TypeController',
	  array('only' => array('index', 'show')));

});


// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);
