<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

	protected $table = 'items';

	protected $fillable = ['title', 'content'];

	public function user() {
		return $this->belongsTo("User");
	}

	public function items() {
		return $this->hasMany("Item");
	}

}
